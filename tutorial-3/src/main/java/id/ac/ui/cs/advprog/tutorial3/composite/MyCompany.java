package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.*;

import java.util.List;

public class MyCompany {

    public static void main(String args[]) {
        Company company;
        Ceo datul;
        Cto fikri;
        BackendProgrammer budi;
        BackendProgrammer ahmad;
        FrontendProgrammer fulan;
        FrontendProgrammer fulanah;
        UiUxDesigner sandi;
        NetworkExpert fijar;
        SecurityExpert kamu;

        company = new Company();

        datul = new Ceo("datul", 500000.00);
        company.addEmployee(datul);

        fikri = new Cto("fikri", 320000.00);
        company.addEmployee(fikri);

        budi = new BackendProgrammer("budi", 94000.00);
        company.addEmployee(budi);

        ahmad = new BackendProgrammer("ahmad", 200000.00);
        company.addEmployee(ahmad);

        fulan = new FrontendProgrammer("fulan",66000.00);
        company.addEmployee(fulan);

        fulanah = new FrontendProgrammer("fulanah", 130000.00);
        company.addEmployee(fulanah);

        sandi = new UiUxDesigner("sandi", 177000.00);
        company.addEmployee(sandi);

        fijar = new NetworkExpert("fijar", 83000.00);
        company.addEmployee(fijar);

        kamu = new SecurityExpert("kamu", 70000.00);
        company.addEmployee(kamu);
        
        List<Employees> allEmployees = company.getAllEmployees();

        for (int index = 0; index < allEmployees.size(); index++) {
            System.out.println(allEmployees.get(index).getName() + " : " + allEmployees.get(index).getRole());
        }

        System.out.println("Total gaji : " + company.getNetSalaries());
    }
}
