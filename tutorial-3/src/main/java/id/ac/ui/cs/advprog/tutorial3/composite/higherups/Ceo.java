package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Ceo extends Employees {
    public Ceo(String name, double salary) {
        //TODO Implement
        this.name = name;
        if (salary < 200000) {
            throw new IllegalArgumentException("Salary must not lower than 200 million");
        } else {
            this.salary = salary;
        }
        this.role = "CEO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
