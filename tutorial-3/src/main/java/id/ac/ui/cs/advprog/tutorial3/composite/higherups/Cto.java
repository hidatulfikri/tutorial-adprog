package id.ac.ui.cs.advprog.tutorial3.composite.higherups;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class Cto extends Employees {
    public Cto(String name, double salary) {
        //TODO Implement
        this.name = name;
        if (salary < 100000) {
            throw new IllegalArgumentException("Salary must not lower than 100 million");
        } else {
            this.salary = salary;
        }
        this.role = "CTO";
    }

    @Override
    public double getSalary() {
        //TODO Implement
        return this.salary;
    }
}
