package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        this.name = name;
        if (salary < 50000) {
            throw new IllegalArgumentException("Salary must not lower than 50 million");
        } else {
            this.salary = salary;
        }
        this.role = "Network Expert";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
