package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;

public class UiUxDesigner extends Employees {
    //TODO Implement
    public UiUxDesigner(String name, double salary) {
        this.name = name;
        if (salary < 90000) {
            throw new IllegalArgumentException("Salary must not lower than 90 million");
        } else {
            this.salary = salary;
        }
        this.role = "UI/UX Designer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
