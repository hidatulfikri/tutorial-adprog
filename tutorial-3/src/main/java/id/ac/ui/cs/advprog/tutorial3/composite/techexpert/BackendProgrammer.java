package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class BackendProgrammer extends Employees {
    //TODO Implement
    public BackendProgrammer(String name, double salary) {
        this.name = name;
        if (salary < 20000) {
            throw new IllegalArgumentException("Salary must not lower than 20 million");
        } else {
            this.salary = salary;
        }
        this.role = "Back End Programmer";
    }

    @Override
    public double getSalary() {
        return this.salary;
    }
}
