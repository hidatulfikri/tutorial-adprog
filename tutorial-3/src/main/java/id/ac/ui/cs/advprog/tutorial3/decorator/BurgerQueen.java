package id.ac.ui.cs.advprog.tutorial3.decorator;

import id.ac.ui.cs.advprog.tutorial3.decorator.bread.*;
import id.ac.ui.cs.advprog.tutorial3.decorator.filling.*;


public class BurgerQueen {

    public static void main(String args[]) {
        Food food = new ThinBunBurger();
        System.out.println(food.getDescription() + " $" + food.cost());

        Food food2 = new ThickBunBurger();
        food2 = new BeefMeat(food2);
        food2 = new Cheese(food2);
        food2 = new ChiliSauce(food2);
        System.out.println(food2.getDescription() + " $" + food2.cost());

        Food food3 = new CrustySandwich();
        food3 = new ChickenMeat(food3);
        food3 = new Cucumber(food3);
        food3 = new Lettuce(food3);
        food3 = new Tomato(food3);
        food3 = new TomatoSauce(food3);
        System.out.println(food3.getDescription() + " $" + food3.cost());
    }
}
