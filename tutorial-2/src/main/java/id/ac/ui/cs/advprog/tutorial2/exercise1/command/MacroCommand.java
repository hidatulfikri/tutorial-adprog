package id.ac.ui.cs.advprog.tutorial2.exercise1.command;

import java.util.*;
import java.util.stream.Collectors;

public class MacroCommand implements Command {

    private List<Command> commands;

    public MacroCommand(Command[] commands) {
        this.commands = Arrays.asList(commands);
    }

    @Override
    public void execute() {
        // TODO Complete me!
        commands.stream()
                .forEach(command -> command.execute());
    }

    @Override
    public void undo() {
        // TODO Complete me!
        commands.stream()
                .collect(Collectors.toCollection(LinkedList::new))
                .descendingIterator()
                .forEachRemaining(command -> command.undo());
    }
}
