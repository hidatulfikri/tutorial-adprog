package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

public class DatulSauce implements Sauce {
    public String toString() {
        return "Datul Sauce";
    }
}
