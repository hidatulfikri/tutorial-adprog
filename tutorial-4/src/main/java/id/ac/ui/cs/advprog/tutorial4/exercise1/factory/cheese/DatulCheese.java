package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class DatulCheese implements Cheese {

    public String toString() {
        return "Datul Cheese";
    }
}
