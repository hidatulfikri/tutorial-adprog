package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

public class DatulVeggy implements Veggies {

    public String toString() {
        return "Datul Veggy";
    }
}
