package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class BlackOlivesTest {
    private BlackOlives blackOlivesClass;

    @Before
    public void setUp() {
        blackOlivesClass = new BlackOlives();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Black Olives", blackOlivesClass.toString());
    }

}