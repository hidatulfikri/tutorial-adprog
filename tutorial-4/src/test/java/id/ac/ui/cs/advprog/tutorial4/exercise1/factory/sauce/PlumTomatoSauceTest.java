package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PlumTomatoSauceTest {
    private PlumTomatoSauce plumTomatoSauceClass;

    @Before
    public void setUp() {
        plumTomatoSauceClass = new PlumTomatoSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Tomato sauce with plum tomatoes", plumTomatoSauceClass.toString());
    }

}