package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ParmesanCheeseTest {
    private ParmesanCheese parmesanCheeseClass;

    @Before
    public void setUp() {
        parmesanCheeseClass = new ParmesanCheese();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Parmesan Cheese", parmesanCheeseClass.toString());
    }

}