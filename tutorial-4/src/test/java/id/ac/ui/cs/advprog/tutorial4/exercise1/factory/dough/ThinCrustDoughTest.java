package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.ThinCrustDough;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThinCrustDoughTest {
    private ThinCrustDough thinCrustDoughClass;

    @Before
    public void setUp() {
        thinCrustDoughClass = new ThinCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Thin Crust Dough", thinCrustDoughClass.toString());
    }

}