package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ThickCrustDoughTest {
    private ThickCrustDough thickCrustDoughClass;

    @Before
    public void setUp() {
        thickCrustDoughClass = new ThickCrustDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("ThickCrust style extra thick crust dough", thickCrustDoughClass.toString());
    }

}