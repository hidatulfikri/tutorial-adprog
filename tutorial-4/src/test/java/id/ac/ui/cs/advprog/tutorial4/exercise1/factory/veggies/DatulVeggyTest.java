package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatulVeggyTest {
    private DatulVeggy datulVeggyClass;

    @Before
    public void setUp() {
        datulVeggyClass = new DatulVeggy();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Datul Veggy", datulVeggyClass.toString());
    }

}