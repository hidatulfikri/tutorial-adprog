package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpinachTest {
    private Class<?> spinachClass;

    @Before
    public void setUp() throws Exception {
        spinachClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Spinach");
    }

    @Test
    public void testSpinachHasToStringMethod() throws Exception {
        Method display = spinachClass.getDeclaredMethod("toString");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("java.lang.String", display.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        assertEquals("Spinach", spinachClass.toString());
    }

}