package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FreshClamsTest {
    private FreshClams freshClamsClass;

    @Before
    public void setUp() {
        freshClamsClass = new FreshClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Fresh Clams from Long Island Sound", freshClamsClass.toString());
    }

}