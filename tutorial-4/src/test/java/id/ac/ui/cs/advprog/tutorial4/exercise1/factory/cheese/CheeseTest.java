package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CheeseTest {
    private Class<?> cheeseClass;

    @Before
    public void setUp() throws Exception {
        cheeseClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese");
    }

    @Test
    public void testCheeseIsAbstract() {
        int classModifiers = cheeseClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testCheeseHasToStringMethod() throws Exception {
        Method display = cheeseClass.getDeclaredMethod("toString");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("java.lang.String", display.getGenericReturnType().getTypeName());
    }
}
