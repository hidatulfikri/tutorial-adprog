package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class RedPepperTest {
    private Class<?> redPepperClass;

    @Before
    public void setUp() throws Exception {
        redPepperClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.RedPepper");
    }

    @Test
    public void testRedPepperHasToStringMethod() throws Exception {
        Method display = redPepperClass.getDeclaredMethod("toString");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("java.lang.String", display.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        assertEquals("Red Pepper", redPepperClass.toString());
    }

}