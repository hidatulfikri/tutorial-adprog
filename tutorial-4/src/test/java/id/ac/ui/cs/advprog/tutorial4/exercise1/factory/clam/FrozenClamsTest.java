package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FrozenClamsTest {
    private FrozenClams frozenClamsClass;

    @Before
    public void setUp() {
        frozenClamsClass = new FrozenClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Frozen Clams from Chesapeake Bay", frozenClamsClass.toString());
    }

}