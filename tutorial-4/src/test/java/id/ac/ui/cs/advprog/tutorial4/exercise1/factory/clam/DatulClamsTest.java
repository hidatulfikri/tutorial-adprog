package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatulClamsTest {
    private DatulClams datulClamsClass;

    @Before
    public void setUp() {
        datulClamsClass = new DatulClams();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Datul Clams", datulClamsClass.toString());
    }

}