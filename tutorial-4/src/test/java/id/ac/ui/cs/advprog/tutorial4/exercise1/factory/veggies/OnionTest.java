package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class OnionTest {
    private Class<?> onionClass;

    @Before
    public void setUp() throws Exception {
        onionClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Onion");
    }

    @Test
    public void testOnionHasToStringMethod() throws Exception {
        Method display = onionClass.getDeclaredMethod("toString");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("java.lang.String", display.getGenericReturnType().getTypeName());
    }

    @Test
    public void testMethodToString() {
        assertEquals("Onion", onionClass.toString());
    }

}