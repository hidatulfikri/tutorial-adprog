package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatulDoughTest {
    private DatulDough datulDoughClass;

    @Before
    public void setUp() {
        datulDoughClass = new DatulDough();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Datul Dough", datulDoughClass.toString());
    }

}