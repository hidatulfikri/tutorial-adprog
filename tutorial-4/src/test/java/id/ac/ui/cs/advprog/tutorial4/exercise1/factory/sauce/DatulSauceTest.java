package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DatulSauceTest {
    private DatulSauce datulSauceClass;

    @Before
    public void setUp() {
        datulSauceClass = new DatulSauce();
    }

    @Test
    public void testMethodToString() {
        assertEquals("Datul Sauce", datulSauceClass.toString());
    }

}