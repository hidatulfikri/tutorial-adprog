package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class VeggiesTest {
    private Class<?> veggiesClass;

    @Before
    public void setUp() throws Exception {
        veggiesClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies");
    }

    @Test
    public void testVeggiesIsAbstract() {
        int classModifiers = veggiesClass.getModifiers();

        assertTrue(Modifier.isAbstract(classModifiers));
    }

    @Test
    public void testClassHasToStringMethod() throws Exception {
        Method display = veggiesClass.getDeclaredMethod("toString");
        int methodModifiers = display.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
        assertEquals("java.lang.String", display.getGenericReturnType().getTypeName());
    }
}
