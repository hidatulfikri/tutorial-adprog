package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.DatulCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.DatulClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.DatulDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.DatulSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.*;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new DatulDough();
    }

    public Sauce createSauce() {
        return new DatulSauce();
    }

    public Cheese createCheese() {
        return new DatulCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new DatulVeggy(), new Spinach(), new Garlic(), new BlackOlives()};
        return veggies;
    }

    public Clams createClam() {
        return new DatulClams();
    }
}
