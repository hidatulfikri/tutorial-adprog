package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import org.junit.Before;
import org.junit.Test;

public class PizzaIngredientFactoryTest {

    private Class<?> pizzaIngredientFactoryClass;

    @Before
    public void setUp() throws Exception {
        pizzaIngredientFactoryClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.exercise1.factory.PizzaIngredientFactory");
    }

    @Test
    public void testPizzaIngredientFactoryIsAPublicInterface() {
        int classModifiers = pizzaIngredientFactoryClass.getModifiers();

        assertTrue(Modifier.isPublic(classModifiers));
        assertTrue(Modifier.isInterface(classModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateDoughAbstractMethod() throws Exception {
        Method createDough = pizzaIngredientFactoryClass.getDeclaredMethod("createDough");
        int methodModifiers = createDough.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateSauceAbstractMethod() throws Exception {
        Method createSauce = pizzaIngredientFactoryClass.getDeclaredMethod("createSauce");
        int methodModifiers = createSauce.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateCheeseAbstractMethod() throws Exception {
        Method createCheese = pizzaIngredientFactoryClass.getDeclaredMethod("createCheese");
        int methodModifiers = createCheese.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateVeggiesAbstractMethod() throws Exception {
        Method createVeggies = pizzaIngredientFactoryClass.getDeclaredMethod("createVeggies");
        int methodModifiers = createVeggies.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }

    @Test
    public void testPizzaIngredientFactoryHasCreateClamAbstractMethod() throws Exception {
        Method createClam = pizzaIngredientFactoryClass.getDeclaredMethod("createClam");
        int methodModifiers = createClam.getModifiers();

        assertTrue(Modifier.isPublic(methodModifiers));
        assertTrue(Modifier.isAbstract(methodModifiers));
    }
}