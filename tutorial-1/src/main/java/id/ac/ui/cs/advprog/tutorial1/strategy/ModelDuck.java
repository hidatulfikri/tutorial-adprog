package id.ac.ui.cs.advprog.tutorial1.strategy;

public class ModelDuck extends Duck {
    // TODO Complete me!

    public ModelDuck() {
        super.setFlyBehavior(new FlyNoWay());
        super.setQuackBehavior(new MuteQuack());
    }

    @Override
    public void display() {
        System.out.println("I'm a Model Duck.");
    }
}
